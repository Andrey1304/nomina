![Nomina](https://img.caminofinancial.com/wp-content/uploads/2020/02/20174728/iStock-1158626456-Copy-1024x732.jpg)
# Título del proyecto:

#### Nomina
***
## Índice
1. [Características 🔗](#características)
2. [Contenido del proyecto 📝](#contenido-del-proyecto)
3. [Tecnologías 💻](#tecnologías)
4. [IDE 🖥](#ide)
5. [Instalación ⚙](#instalación)
6. [Demo 📲](#demo)
7. [Autor(es) ✍](#autores)
8. [Institución Académica 🎓](#institución-académica)
9. [Referencias 🔎](#institución-académica)
***


#### Características:

  - Proyecto con registro y búsqueda de empleado en una base de datos al igual el proyecto cuenta con una sesión de concepto de la nomina
  - Proyecto implementa el patrón de arquitectura de software Modelo-vista-controlador (MVC)
  - Proyecto implementa la persistencia de datos y el patron DAO
  - Versión: [![Versión](https://img.shields.io/badge/-2.0-lightgrey)](#)
***
  #### Contenido del proyecto

| Archivo      | Descripción  |
|--------------|--------------|
| [index.html](https://gitlab.com/Andrey1304/nomina/-/blob/master/WebContent/index.html) | Archivo principal con botones para el registro, búsqueda del empleado y concepto de nomina|
| [consulta.jsp](https://gitlab.com/Andrey1304/nomina/-/blob/master/WebContent/consulta.jsp) | Archivo JSP para realizar la consulta del empleado y con la conexion al controlador|
| [listado.jsp](https://gitlab.com/Andrey1304/nomina/-/blob/master/WebContent/listado.jsp) | Archivo JSP para la consulta del concepto y con la conexion al controlador|
| [registro.jsp](https://gitlab.com/Andrey1304/nomina/-/blob/master/WebContent/registro.jsp) | Archivo JSP para el registro del empleado y con la conexion al controlador|
| [resultados.jsp](https://gitlab.com/Andrey1304/nomina/-/blob/master/WebContent/resultados.jsp) | Archivo JSP para la vista de la consulta del empleado y con la conexion al controlador|
| [Controladores](https://gitlab.com/Andrey1304/nomina/-/tree/master/src/nomina/controller) | Archivos Java con el controlador de consulta y registro|
| [Entidades](https://gitlab.com/Andrey1304/nomina/-/tree/master/src/nomina/entities) | Archivos Java con los atributos de los objetos:Concepto, Empleado, Liquidacion, Proceso, Tercero, Tipo de concepto |
| [DAO](https://gitlab.com/Andrey1304/nomina/-/tree/master/src/nomina/entities) | Archivos Java con el patrón DAO en cada entidad y una interface  GenericDao|
| [Conexion.java](https://gitlab.com/Andrey1304/nomina/-/blob/master/src/nomina/util/Conexion.java) | Archivo Java con el proceso conexion con la base de datos|
| [Prueba.java](https://gitlab.com/Andrey1304/nomina/-/blob/master/src/nomina/util/Prueba.java) | Archivo Java donde se implementa una prueba del aplicativo web|
| [persistence.xml](https://gitlab.com/Andrey1304/nomina/-/blob/master/src/META-INF/persistence.xml) | Archivo XML con las propiedades de la persistencia en los datos,la especififcacion del gestor de base de datos y su usuario y contraseña, la URL de la base de datos y los drivers necesarios|
| [pom.xml](https://gitlab.com/Andrey1304/nomina/-/blob/master/pom.xml) | Archivo XML con las dependencias usadas en el proyecto|

  
***
#### Tecnologías
  - [![Java Web](https://img.shields.io/badge/-Java%20Web-green)](https://www.java.com/es/download/help/java_webstart_es.html)
  - [![MySQL](https://img.shields.io/badge/-MySQL-yellow)](https://www.mysql.com/)
  - [![MAVEN](https://img.shields.io/badge/-MAVEN-blue)](https://maven.apache.org/)
  - [![JPA](https://img.shields.io/badge/-JPA-red)](https://es.wikipedia.org/wiki/Java_Persistence_API)
  - [![HTML5](https://img.shields.io/badge/HTML5-CSS-green)](https://developer.mozilla.org/es/docs/Web/Guide/HTML/HTML5)
  - [![Bootstrap 4.0](https://img.shields.io/badge/-Bootstrap%204.0-blueviolet)](https://getbootstrap.com/docs/4.0/getting-started/introduction/)
  - [![JavaScript](https://img.shields.io/badge/JavaScript-green)](https://developer.mozilla.org/es/docs/Web/JavaScript)
  


Usted puede ver el siguiente marco conceptual sobre Java Web:

  - [Vídeo explicativo  desarrollo aplicaciones web con Java web](https://www.youtube.com/watch?v=xPCrf80hgE8)
  - [Vídeo explicativo  desarrollo aplicaciones web con Java web,JSF,JPA,Maven()](https://www.youtube.com/watch?v=dp06qVE48n0)
  - [Gúia de desarrollo web implementando JPA y Java web](https://joseltoro.blogspot.com/2020/04/crear-un-proyecto-jpa-java-web-usando.html)
  
  ***
#### IDE

- El proyecto se desarrolla usando Eclipse IDE, Eclipse IDE es una plataforma de desarrollo, diseñada para ser extendida de forma indefinida a través de plug-ins, Es uno de los entornos Java más utilizados a nivel profesional [Rodríguez, A. (2009)](#https://www.aprenderaprogramar.com/index.php?option=com_content&view=article&id=398:netbeans-eclipse-jcreator-jbuilder-icual-es-el-mejor-entorno-de-desarrollo-ide-para-java-cu00613b&catid=68&Itemid=188).


***
### Instalación

Es necesario tener instalado un navegador web, recomendamos Descargar Google Chrome -> [descargar](https://www.google.com/intl/es/chrome/?brand=UUXU&gclid=CjwKCAjwg4-EBhBwEiwAzYAlsikFQNC060oYLuGkUVy44X52BoSxpm0KOyjfsNrF0eV7ENvSwekQ-BoCskUQAvD_BwE&gclsrc=aw.ds)


1. Local  
 - Descarga el repositorio ubicado en [descargar](https://gitlab.com/Andrey1304/nomina) 
 - Ejecutar el Apache XAMPP y MySQL
 - Ejecutar el archivo con el IDE Eclipse para asi poder ejecutarlo sin errores.

2. GitLab   
 - Realizando un fork  

***
### Demo
Para ver el demo de la aplicación puede dirigirse a:  [Nomina](https://gitlab.com/Andrey1304/nomina/-/tree/master/WebContent/DEMO)
***
### Autor(es)
Proyecto desarrollado por [Brayam Andrey Oliveros Rivera](<BrayamAndreyOR@ufps.edu.co>).


***
### Institución Académica   
Proyecto desarrollado en la Materia programación web del  [Programa de Ingeniería de Sistemas] de la [Universidad Francisco de Paula Santander]

  [Programa de Ingeniería de Sistemas]: <https://ingsistemas.cloud.ufps.edu.co/>
  [Universidad Francisco de Paula Santander]: <https://ww2.ufps.edu.co/>
   
   ***
### Referencias
###### Arquitectura Java Web Start para desplegar clientes de aplicaciones. (2021, 22 enero). ibm.https://www.ibm.com/docs/es/was/9.0.5?topic=start-java-web-architecture-deploying-application-clients

###### Rodríguez, A. (2009). Eclipse IDE Entorno de desarrollo integrado para Java. aprenderaprogramar. http://www.edu4java.com/es/java/eclipse-ide-entorno-de-desarrollo.html
